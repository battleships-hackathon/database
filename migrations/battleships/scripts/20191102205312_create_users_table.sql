-- // create users table
-- Migration SQL that makes the change goes here.

CREATE TABLE users (
    id    SERIAL NOT NULL PRIMARY KEY,
    name  TEXT   NOT NULL,
    host  TEXT   NOT NULL,
    token TEXT   NOT NULL UNIQUE
);

CREATE INDEX i_users__token ON users(token);

-- //@UNDO
-- SQL to undo the change goes here.

DROP INDEX i_users__token;
DROP TABLE users;