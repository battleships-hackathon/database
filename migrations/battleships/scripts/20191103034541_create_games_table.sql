-- // create games table
-- Migration SQL that makes the change goes here.

CREATE TABLE games (
    id          SERIAL    NOT NULL PRIMARY KEY,
    player_id   INT       NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    opponent_id INT       NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    status      TEXT      NOT NULL,
    start_date  TIMESTAMP NULL,
    finish_date TIMESTAMP NULL
);

-- //@UNDO
-- SQL to undo the change goes here.

DROP TABLE games;