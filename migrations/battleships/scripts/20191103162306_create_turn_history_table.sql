-- // create turn_history table
-- Migration SQL that makes the change goes here.

CREATE TABLE turn_history (
    game_id  INT  NOT NULL REFERENCES games(id) ON UPDATE CASCADE ON DELETE CASCADE,
    number   INT  NOT NULL,
    user_id  INT  NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    abscissa INT  NOT NULL,
    ordinate INT  NOT NULL,
    result   TEXT NOT NULL,
    PRIMARY KEY (game_id, number)
);

CREATE FUNCTION next_game_turn_number(INT) RETURNS INT
    LANGUAGE SQL AS 'SELECT coalesce((SELECT max(th.number) + 1 FROM turn_history th WHERE th.game_id = $1), 0);';

-- //@UNDO
-- SQL to undo the change goes here.

DROP FUNCTION next_game_turn_number(INT);
DROP TABLE turn_history;