-- // create battlefields table
-- Migration SQL that makes the change goes here.

CREATE TABLE battlefields (
    game_id     INT     NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    user_id     INT     NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    arrangement INT[][] NOT NULL,
    PRIMARY KEY (game_id, user_id)
);

-- //@UNDO
-- SQL to undo the change goes here.

DROP TABLE battlefields;
